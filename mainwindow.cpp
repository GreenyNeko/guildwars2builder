#include "mainwindow.h"
#include "ui_mainwindow.h"

// for messages
#include <QMessageBox>
// for linking to websites
#include <QDesktopServices>
#include <QUrl>
// for copy/pasting builds
#include <QClipboard>
// for saving and loading
#include <QFile>
#include <QFileDialog>
#include <QTextStream>

#include "dialogsettings.h"
#include "templateparser.h"


// for handling keys such as delete
#include <QKeyEvent>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    loadConfig();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::actionNewSetTriggered()
{
    this->currentFilePath = QString();
    QMessageBox::StandardButton reply = QMessageBox::question(this, "Create New Set", "Creating a new set will clear all builds.\n\nAre you sure?");
    if(reply == QMessageBox::Yes)
    {
        ui->buildTable->setRowCount(0);
    }
    updateBuildCount();
}

void MainWindow::actionLoadSetTriggered()
{
    QString path = QFileDialog::getOpenFileName(this, tr("Load Build Set"), "", tr("Build Set (*.set)"));
    loadSetFile(path);
    this->currentFilePath = path;
    updateBuildCount();
}

void MainWindow::actionSaveSetTriggered()
{
    if(this->currentFilePath.isEmpty())
    {
        this->currentFilePath = QFileDialog::getSaveFileName(this, tr("Save Build Set"), tr("default.set"), tr("Build Set (*.set)"));
    }
    saveSetFile();
}

void MainWindow::actionNewBuildTriggered()
{
    QMessageBox msgBox;
    msgBox.setText("This feature is not available");
    msgBox.exec();
    updateBuildCount();
}

void MainWindow::actionPasteFromClipboardTriggered()
{
    QClipboard *cp = QApplication::clipboard();
    QString cbContent = cp->text();
    if(!cbContent.startsWith("[&") || !cbContent.endsWith("=]") || cbContent.length() != 63)
    {
        QMessageBox::warning(this, "Invalid Build Code", "Build codes need to be 63 characters long, start with \"[&\" and end with \"=]\"!");
        return;
    }
    TemplateParser templateParser(cbContent);
    fillBuildTableRow(templateParser.getClassFromClassIdentifier(), "PvX", "*New", "", cbContent, 0);
}

void MainWindow::actionAboutTriggered()
{
    QMessageBox::about(this, "Guild Wars 2 Builder - About",
                       "Guild Wars 2 Builder is a project written in C++ using Qt.\n"
                       "The main goal of this project is to store and manage builds externally in a way\n"
                       "that makes it easy to swap out builds from in-game.\n\n"
                       "The project was created by GreenyNeko, a game developer and content creator.\n");
}

void MainWindow::actionReportBugTriggered()
{
    QUrl qUrl = QUrl();
    qUrl.setUrl("https://bitbucket.org/GreenyNeko/guildwars2builder/issues?status=new&status=open");
    QDesktopServices::openUrl(qUrl);
}

void MainWindow::actionSourceCodeTriggered()
{
    QUrl qUrl = QUrl();
    qUrl.setUrl("https://bitbucket.org/GreenyNeko/guildwars2builder/src/master/");
    QDesktopServices::openUrl(qUrl);
}

void MainWindow::actionPaypalTriggered()
{
    QUrl qUrl = QUrl();
    qUrl.setUrl("https://paypal.com/paypalme/greenyneko");
    QDesktopServices::openUrl(qUrl);
}

void MainWindow::actionSettingsTriggered()
{
    DialogSettings *dialogSettings = new DialogSettings(this);
    dialogSettings->show();
    dialogSettings->exec();
}

void MainWindow::actionPatreonTriggered()
{
    QUrl qUrl = QUrl();
    qUrl.setUrl("https://www.patreon.com/GreenyNeko");
    QDesktopServices::openUrl(qUrl);
}

void MainWindow::actionYoutubeTriggered()
{
    QUrl qUrl = QUrl();
    qUrl.setUrl("https://www.youtube.com/user/GreenNekoHaunt");
    QDesktopServices::openUrl(qUrl);
}

void MainWindow::actionBlogTriggered()
{
    QUrl qUrl = QUrl();
    qUrl.setUrl("https://www.greenyneko.com/");
    QDesktopServices::openUrl(qUrl);
}

void MainWindow::actionSaveSetAsTriggered()
{
    this->currentFilePath = QFileDialog::getSaveFileName(this, tr("Save Build Set"), tr("default.set"), tr("Build Set (*.set)"));
    saveSetFile();
}

void MainWindow::filterBuildTable(QString searchStr)
{
    if(searchStr.isEmpty())
    {
        for(int i = 0; i < ui->buildTable->rowCount(); i++)
        {
            ui->buildTable->showRow(i);
        }
    }
    else
    {
        for(int i = 0; i < ui->buildTable->rowCount(); i++)
        {
            QString prof = ui->buildTable->item(i, 0)->text();
            QString mode = ui->buildTable->item(i, 1)->text();
            QString name = ui->buildTable->item(i, 2)->text();
            QString note = ui->buildTable->item(i, 3)->text();
            if(mode.contains(searchStr) || prof.contains(searchStr) || name.contains(searchStr) || note.contains(searchStr))
            {
                ui->buildTable->showRow(i);
            }
            else
            {
                ui->buildTable->hideRow(i);
            }
        }
    }
}

void MainWindow::actionAboutQtTriggered()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::buildTableContextMenu(QPoint pos)
{
    QMenu contextMenu(tr("Context menu"), this);
    QAction actionCopyBuildTemplate("Copy Build Template", this);
    QObject::connect(&actionCopyBuildTemplate, SIGNAL(triggered()), this, SLOT(actionCopyBuildCode()));
    QAction actionPasteBuildTemplate("Paste Build Template", this);
    QObject::connect(&actionPasteBuildTemplate, SIGNAL(triggered()), this, SLOT(actionPasteFromClipboardTriggered()));
    QAction actionDeleteBuildTemplate("Delete Build Template", this);
    QObject::connect(&actionDeleteBuildTemplate, SIGNAL(triggered()), this, SLOT(actionDeleteBuildCode()));
    if(ui->buildTable->selectedItems().length() == 0)
    {
        actionCopyBuildTemplate.setEnabled(false);
        actionDeleteBuildTemplate.setEnabled(false);
    }
    else
    {
        actionCopyBuildTemplate.setEnabled(true);
        actionDeleteBuildTemplate.setEnabled(true);
    }
    contextMenu.addAction(&actionCopyBuildTemplate);
    contextMenu.addAction(&actionPasteBuildTemplate);
    contextMenu.addAction(&actionDeleteBuildTemplate);
    contextMenu.exec(mapToGlobal(pos));
}

void MainWindow::actionDeleteBuildCode()
{
    QList<QTableWidgetItem*> items = ui->buildTable->selectedItems();
    QList<int> rows;
    for(QTableWidgetItem* item : items)
    {
        if(!rows.contains(item->row()))
        {
           rows.append(item->row());
        }
    }
    for(int row : rows)
    {
        ui->buildTable->removeRow(row);
    }
    updateBuildCount();
}

void MainWindow::actionCopyBuildCode()
{
    QClipboard* qCb = QApplication::clipboard();
    int selectedRow = ui->buildTable->selectedItems()[0]->row();
    qCb->setText(ui->buildTable->item(selectedRow, 4)->text());
}

void MainWindow::keyPressEvent(QKeyEvent* e)
{
    if(ui->buildTable->hasFocus())
    {
        switch(e->key())
        {
            case Qt::Key_Delete:
            {
                QList<QTableWidgetItem*> items = ui->buildTable->selectedItems();
                QList<int> rows;
                for(QTableWidgetItem* item : items)
                {
                    if(!rows.contains(item->row()))
                    {
                       rows.append(item->row());
                    }
                }
                for(int row : rows)
                {
                    ui->buildTable->removeRow(row);
                }
                updateBuildCount();
                break;
            }
            case Qt::Key_C:
            {
                if(e->modifiers() & Qt::ControlModifier)
                {
                    if(ui->buildTable->hasFocus())
                    {
                        QClipboard* qCb = QApplication::clipboard();
                        int selectedRow = ui->buildTable->selectedItems()[0]->row();
                        qCb->setText(ui->buildTable->item(selectedRow, 4)->text());
                    }
                }
                break;
            }
        }
    }
}

void MainWindow::fillBuildTableRow(QString prof, QString mode, QString name, QString note, QString code, int at=0)
{
    QSettings qSettings("GreenyNeko", "Guild Wars 2 Builder");
    int strategy = qSettings.value("general/insertionStrategy", 0).value<int>();
    int insertionAt = at;
    switch(strategy)
    {
        case 3:
            {
                if(ui->buildTable->selectedItems().length() > 0)
                {
                    int row = ui->buildTable->selectedItems()[0]->row();
                    insertionAt = row + 1;
                }
                else
                {
                    insertionAt = at;
                }
                break;
            }
        case 2:
            {
                if(ui->buildTable->selectedItems().length() > 0)
                {
                    int row = ui->buildTable->selectedItems()[0]->row();
                    insertionAt = row;
                }
                else
                {
                    insertionAt = at;
                }
                break;
            }
        case 1:
            {
                insertionAt = ui->buildTable->rowCount();
                break;
            }
        default:
            insertionAt = at;
    }
    ui->buildTable->insertRow(insertionAt);
    QTableWidgetItem* profItem = new QTableWidgetItem(prof);
    profItem->setBackgroundColor(QColor(225,225,225));
    profItem->setFlags(profItem->flags() ^ Qt::ItemIsEditable);
    ui->buildTable->setItem(insertionAt, 0, profItem);
    ui->buildTable->setItem(insertionAt, 1, new QTableWidgetItem(mode));
    ui->buildTable->setItem(insertionAt, 2, new QTableWidgetItem(name));
    ui->buildTable->setItem(insertionAt, 3, new QTableWidgetItem(note));
    QTableWidgetItem* previewItem = new QTableWidgetItem(code);
    previewItem->setFlags(previewItem->flags() ^ Qt::ItemIsEditable);
    previewItem->setBackgroundColor(QColor(225,225,225));
    ui->buildTable->setItem(insertionAt, 4, previewItem);

    updateBuildCount();
}

void MainWindow::saveSetFile()
{
    QFile qFile(this->currentFilePath);
    if(!qFile.open(QIODevice::WriteOnly))
    {
        QMessageBox::warning(this, tr("Unable to create file"), qFile.errorString());
        return;
    }
    QTextStream fileIn(&qFile);
    for(int i = 0; i < ui->buildTable->rowCount(); i++)
    {
        for(int j = 1; j < ui->buildTable->columnCount(); j++)
        {
            fileIn << ui->buildTable->item(i, j)->text();
            fileIn << ";";
        }
        fileIn << "\n";
    }
}

void MainWindow::loadSetFile(QString path)
{
    QFile qFile(path);
    if(!qFile.open(QIODevice::ReadOnly))
    {
        QMessageBox::warning(this, tr("Unable to open file"), qFile.errorString());
        return;
    }
    QTextStream out(&qFile);
    ui->buildTable->setRowCount(0); // clear for loading
    int currLine = 0;
    while(!out.atEnd())
    {
        QString line = out.readLine();
        QStringList strList = line.split(";");
        TemplateParser templateParser(strList[strList.length() - 2]);
        fillBuildTableRow(templateParser.getClassFromClassIdentifier(), strList[0], strList[1], strList[2], strList[3], 0);
        currLine++;
    }
}

void MainWindow::updateBuildCount()
{
    ui->buildCountLabel->setText("Builds: " + QString::number(ui->buildTable->rowCount()));
}

void MainWindow::loadConfig()
{
    QSettings qSettings("GreenyNeko", "Guild Wars 2 Builder");
    QString defaultPath = qSettings.value("general/defaultPath").toString();
    if(qSettings.value("general/useDefaultPath", false).value<bool>())
    {
        loadSetFile(defaultPath);
    }
}
