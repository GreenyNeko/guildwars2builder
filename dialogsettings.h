#ifndef DIALOGSETTINGS_H
#define DIALOGSETTINGS_H

#include <QDialog>
#include <QAbstractButton>

#include <QSettings>

namespace Ui {
class DialogSettings;
}

class DialogSettings : public QDialog
{
    Q_OBJECT

public:
    explicit DialogSettings(QWidget *parent = nullptr);
    ~DialogSettings() override;

public slots:
    void accept() override;
    void onDefaultSetSelectFileClicked();
    void onResponseButtonClicked(QAbstractButton*);
    void onCheckBoxUseDefaultSetChanged(int);

private:
    void applyChanges();
    void updateConfigFile();

    Ui::DialogSettings *ui;
    QString defaultPath;
    QSettings qSettings;
};

#endif // DIALOGSETTINGS_H
