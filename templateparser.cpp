#include "templateparser.h"

TemplateParser::TemplateParser()
    : buildCode("[&DQYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=]")
{

}

TemplateParser::TemplateParser(QString buildCode)
    : buildCode(buildCode)
{
    // set to default if invalid
    if(buildCode.length() != 63
            || !buildCode.startsWith("[&") || !buildCode.endsWith("=]"))
    {
        buildCode = "[&DQYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=]";
    }
}

QString TemplateParser::getClassFromClassIdentifier()
{
    QString classIdentifier = buildCode.at(4);
    QString profession = "INVALID";
    if(classIdentifier == "g")
    {
        profession = "Necromancer";
    }
    else if(classIdentifier == "I")
    {
        profession = "Warrior";
    }
    else if(classIdentifier == "E")
    {
        profession = "Guardian";
    }
    else if(classIdentifier == "U")
    {
        profession = "Thief";
    }
    else if(classIdentifier == "k")
    {
        profession = "Revenant";
    }
    else if(classIdentifier == "c")
    {
        profession = "Mesmer";
    }
    else if(classIdentifier == "Q")
    {
        profession = "Ranger";
    }
    else if(classIdentifier == "Y")
    {
        profession = "Elementalist";
    }
    else if(classIdentifier == "M")
    {
        profession = "Engineer";
    }
    return profession;
}
