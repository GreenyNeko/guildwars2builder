#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidgetItem> //needed for some weird reason..



QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void actionNewSetTriggered();
    void actionLoadSetTriggered();
    void actionSaveSetTriggered();
    void actionNewBuildTriggered();
    void actionPasteFromClipboardTriggered();
    void actionAboutTriggered();
    void actionReportBugTriggered();
    void actionSourceCodeTriggered();
    void actionPaypalTriggered();
    void actionSettingsTriggered();
    void actionPatreonTriggered();
    void actionYoutubeTriggered();
    void actionBlogTriggered();
    void actionSaveSetAsTriggered();
    void filterBuildTable(QString);
    void actionAboutQtTriggered();
    void buildTableContextMenu(QPoint);
    void actionDeleteBuildCode();
    void actionCopyBuildCode();

protected:
    void keyPressEvent(QKeyEvent* event);

private:
    void fillBuildTableRow(QString prof, QString mode, QString name, QString note, QString code, int at);

    void saveSetFile();
    void loadSetFile(QString path);
    void updateBuildCount();
    void loadConfig();

    Ui::MainWindow *ui;
    QString currentFilePath;
};
#endif // MAINWINDOW_H
