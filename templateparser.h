#ifndef TEMPLATEPARSER_H
#define TEMPLATEPARSER_H

#include <QString>

class TemplateParser
{
public:
    TemplateParser();
    TemplateParser(QString buildCode);
    QString getClassFromClassIdentifier();

private:
    QString buildCode;
};

#endif // TEMPLATEPARSER_H
