#include "dialogsettings.h"
#include "ui_dialogsettings.h"

// required to get a file path
#include <QFileDialog>
// for settings
#include <QSettings>

DialogSettings::DialogSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogSettings),
    qSettings("GreenyNeko", "Guild Wars 2 Builder")
{
    ui->setupUi(this);

    ui->valueDefaultSet->setText(qSettings.value("general/defaultPath").value<QString>());
    ui->buttonBox->button(QDialogButtonBox::Apply)->setEnabled(false);
    ui->checkBoxUseDefaultSet->setChecked(qSettings.value("general/useDefaultPath", false).value<bool>());
    int strategy = qSettings.value("general/insertionStrategy", 0).value<int>();
    switch(strategy)
    {
        case 3:
            {
                ui->radioButtonBelowSelected->setChecked(true);
                break;
            }
        case 2:
            {
                ui->radioButtonAboveSelected->setChecked(true);
                break;
            }
        case 1:
            {
                ui->radioButtonAtBottom->setChecked(true);
                break;
            }
        default:
            ui->radioButtonAtTop->setChecked(true);
    }
}

DialogSettings::~DialogSettings()
{
    delete ui;
}

void DialogSettings::accept()
{
    applyChanges();
    this->close(); // does it do it on it's own?
}

void DialogSettings::onDefaultSetSelectFileClicked()
{
    this->defaultPath = QFileDialog::getOpenFileName(this, tr("Default Build Set"), tr("default.set"), tr("Build Set (*.set)"));
    if(this->defaultPath != qSettings.value("general/defaultPath").toString())
    {
        ui->buttonBox->button(QDialogButtonBox::Apply)->setEnabled(true);
    }
    ui->valueDefaultSet->setText(this->defaultPath);
}

void DialogSettings::onResponseButtonClicked(QAbstractButton* abstrButton)
{
    if(abstrButton == ui->buttonBox->button(QDialogButtonBox::Apply))
    {
        applyChanges();
        ui->buttonBox->button(QDialogButtonBox::Apply)->setEnabled(false);
    }
}

void DialogSettings::onCheckBoxUseDefaultSetChanged(int state)
{
    if(state == Qt::CheckState::Checked)
    {
        ui->valueDefaultSet->setEnabled(true);
        ui->buttonDefaultSetSelectFile->setEnabled(true);
    }
    else if(state == Qt::CheckState::Unchecked)
    {
        ui->valueDefaultSet->setEnabled(false);
        ui->buttonDefaultSetSelectFile->setEnabled(false);
    }
}

void DialogSettings::applyChanges()
{
    if(ui->valueDefaultSet->text().isEmpty())
    {
        qSettings.setValue("general/useDefaultPath", false);
    }
    else
    {
        qSettings.setValue("general/useDefaultPath", ui->checkBoxUseDefaultSet->isChecked());
    }
    qSettings.setValue("general/defaultPath", ui->valueDefaultSet->text());
    int strategy = 0;
    if(ui->radioButtonAtTop->isChecked())
    {
        strategy = 0;
    }
    else if(ui->radioButtonAtBottom->isChecked())
    {
        strategy = 1;
    }
    else if(ui->radioButtonAboveSelected->isChecked())
    {
        strategy = 2;
    }
    else if(ui->radioButtonBelowSelected->isChecked())
    {
        strategy = 3;
    }
    qSettings.setValue("general/insertionStrategy", strategy);
    qSettings.sync();
}
